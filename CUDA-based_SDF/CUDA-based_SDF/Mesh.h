#ifndef ________MESH__HHH___________
#define ________MESH__HHH___________

#include <stdio.h>
#include <string>
#pragma warning(disable:4996)

#include "Vertex.h"
#include "Triangle.h"
#include "HScommon.h"
#include "Vector3.h"

class Mesh
{
public:

	unsigned int		m_numVerts;
	unsigned int		m_numFaces;
	bool				m_debug;
	Vector3		 		m_v3Result_T;
	Vertex	    *		m_pVertices;
	Triangle    *		m_pFaces;

	Mesh();
	~Mesh();
	
	void debugOut(char * str, char * format);
	void setDebug(bool bflag);	
	
	void meshRead(std::string a);
	Vector3 getTcentroid(unsigned int Tid);

	Vector3  VertexAdjacency(Triangle t, unsigned int ui);
	void FindNeighborTriangles();

};
#endif // !________MESH__HHH___________
