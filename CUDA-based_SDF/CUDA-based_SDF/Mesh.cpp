#include <float.h>
#include "Mesh.h"
#include "Vertex.h"
//#define ____HS__DEBUG

Mesh::Mesh()
{
	m_pVertices = NULL;
	m_pFaces = NULL;
	//m_pBB = NULL;

	setDebug(false);
}

Mesh::~Mesh()
{
	SAFEDELETEARRAY(m_pVertices);
	SAFEDELETEARRAY(m_pFaces);
	//SAFEDELETEARRAY(m_pBB);
}
/*
Vector3 Mesh::getTcentroid(unsigned int Tid)
{
	Vector3 v3Result_T;
	//v3Result_T = (0.0f, 0.0f, 0.0f);
	v3Result_T.x = 0.0f;
	v3Result_T.y = 0.0f;
	v3Result_T.z = 0.0f;

	if (Tid > m_numFaces - 1)
	{
		printf("\nError : Tid is bigger  than the actual number of triangles.");
		return v3Result_T;
	}


	for (int i = 0; i < 3; i++)
	{
		v3Result_T.x += m_pVertices[m_pFaces[Tid].m_v[i]].x;
		v3Result_T.y += m_pVertices[m_pFaces[Tid].m_v[i]].y;
		v3Result_T.z += m_pVertices[m_pFaces[Tid].m_v[i]].z;
		//printf("%d %d %d\n", m_V[i].x, m_V[i].y, m_V[i].z);
	}
	v3Result_T.x /= 3.0f;
	v3Result_T.y /= 3.0f;
	v3Result_T.z /= 3.0f;

	//printf("%f\n%f\n%f\n\n", v3Result_T.x, v3Result_T.y, v3Result_T.z);
	return v3Result_T;
}
*/
void Mesh::setDebug(bool bflag)
{
	m_debug = bflag;
}

void Mesh::debugOut(char *str, char *format)
{
	if (!m_debug)
		return;

	printf(str, format);
}

void Mesh:: meshRead(std::string a)
{
	char format[100];
	char prop[100];
	unsigned int dummy;

	/*std::string git_dir(getenv("GITDIR"));
	git_dir += "/0000.ply";*/

	FILE * fp = fopen(a.c_str(), "r");

	fgets(format, sizeof(format), fp);
	debugOut("%s", format);
	fgets(format, sizeof(format), fp);
	debugOut("%s", format);
	fgets(format, sizeof(format), fp);
	debugOut("%s", format);

	fscanf(fp, "%s %s %d\n", format, prop, &m_numVerts);
#ifdef ____HS__DEBUG
	printf("%s %s %d\n", format, prop, m_numVerts);
#endif // !____HS__DEBUG
	
	m_pVertices = new Vertex[m_numVerts];
	//m_pVertices = new Vector3[m_numVerts];

	fgets(format, sizeof(format), fp);
	debugOut("%s", format);
	fgets(format, sizeof(format), fp);
	debugOut("%s", format);
	fgets(format, sizeof(format), fp);
	debugOut("%s", format); 
	
	////rgba
	fgets(format, sizeof(format), fp);
	debugOut("%s", format);
	fgets(format, sizeof(format), fp);
	debugOut("%s", format);
	fgets(format, sizeof(format), fp);
	debugOut("%s", format);
	fgets(format, sizeof(format), fp);
	debugOut("%s", format);

	fscanf(fp, "%s %s %d\n", format, prop, &m_numFaces);
#ifdef ____HS__DEBUG
	printf("%s %s %d\n", format, prop, m_numFaces);
#endif // !____HS__DEBUG
	
	m_pFaces = new Triangle[m_numFaces];

	fgets(format, sizeof(format), fp);
	debugOut("%s", format);
	fgets(format, sizeof(format), fp);
	debugOut("%s", format);
	
	for (unsigned int i = 0; i < m_numVerts; i++)
	{
		fscanf(fp, "%f %f %f %d %d %d %d\n", &m_pVertices[i].x, &m_pVertices[i].y, &m_pVertices[i].z, &m_pVertices[i].r, &m_pVertices[i].g, &m_pVertices[i].b, &m_pVertices[i].a );
		//fscanf(fp, "%f %f %f\n", &m_pVertices[i].x, &m_pVertices[i].y, &m_pVertices[i].z);
#ifdef ____HS__DEBUG
		printf("%f %f %f %d %d %d %d\n", m_pVertices[i].x, m_pVertices[i].y, m_pVertices[i].z, m_pVertices[i].r, m_pVertices[i].g, m_pVertices[i].b, m_pVertices[i].a);
#endif // !____HS__DEBUG
	}

	for (unsigned int i = 0; i < m_numFaces; i++)
	{
		fscanf(fp, "%d %d %d %d", &dummy, &m_pFaces[i].m_ui[0], &m_pFaces[i].m_ui[1], &m_pFaces[i].m_ui[2]);
#ifdef ____HS__DEBUG
		printf("%d %d %d %d\n", dummy, m_pFaces[i].m_ui[0], m_pFaces[i].m_ui[1], m_pFaces[i].m_ui[2]);
#endif // !____HS__DEBUG
	}

	fclose(fp);
	
	for(unsigned int tid=0; tid<m_numFaces; tid++)
	{
		for(int j=0; j<3; j++)
		{
			m_pFaces[tid].m_v[j].x = m_pVertices[m_pFaces[tid].m_ui[j]].x;
			m_pFaces[tid].m_v[j].y = m_pVertices[m_pFaces[tid].m_ui[j]].y;
			m_pFaces[tid].m_v[j].z = m_pVertices[m_pFaces[tid].m_ui[j]].z;
		}
	}
	
	for(unsigned int k=0; k<m_numFaces; k++)
	{
		m_pFaces[k].m_e[2].x = m_pFaces[k].m_v[1].x - m_pFaces[k].m_v[0].x;
		m_pFaces[k].m_e[2].y = m_pFaces[k].m_v[1].y - m_pFaces[k].m_v[0].y;
		m_pFaces[k].m_e[2].z = m_pFaces[k].m_v[1].z - m_pFaces[k].m_v[0].z;

		m_pFaces[k].m_e[1].x = m_pFaces[k].m_v[0].x - m_pFaces[k].m_v[2].x;
		m_pFaces[k].m_e[1].y = m_pFaces[k].m_v[0].y - m_pFaces[k].m_v[2].y;
		m_pFaces[k].m_e[1].z = m_pFaces[k].m_v[0].z - m_pFaces[k].m_v[2].z;

		m_pFaces[k].m_e[0].x = m_pFaces[k].m_v[2].x - m_pFaces[k].m_v[1].x;
		m_pFaces[k].m_e[0].y = m_pFaces[k].m_v[2].y - m_pFaces[k].m_v[1].y;
		m_pFaces[k].m_e[0].z = m_pFaces[k].m_v[2].z - m_pFaces[k].m_v[1].z;

		
		float n_x = (-m_pFaces[k].m_e[1].y * m_pFaces[k].m_e[2].z) - (-m_pFaces[k].m_e[1].z * m_pFaces[k].m_e[2].y);
		m_pFaces[k].m_normals.x = n_x/fabs(n_x);
		
		float n_y = (-m_pFaces[k].m_e[1].x * m_pFaces[k].m_e[2].z) - (-m_pFaces[k].m_e[1].z * m_pFaces[k].m_e[2].x);
		m_pFaces[k].m_normals.y = n_y/fabs(n_y);
		
		float n_z = (-m_pFaces[k].m_e[1].x * m_pFaces[k].m_e[2].y) - (-m_pFaces[k].m_e[1].y * m_pFaces[k].m_e[2].x);
		m_pFaces[k].m_normals.z = n_z/fabs(n_z);

		//printf(">> %f, %f, %f\n",m_pFaces[k].m_normals.x, m_pFaces[k].m_normals.y, m_pFaces[k].m_normals.z);
	}
	
	/////////////////////////////////////// Setting Vertex vector with surround Triangles
	for(int i=0; i<m_numFaces; i++)
	{
		Triangle * pTris = &m_pFaces[i];
		pTris->m_uiMe = i;	///////indexing

		for(int k=0; k<3; k++)
			pTris->v(k, m_pVertices)->AddNeighbor(pTris);
	}
	
	FindNeighborTriangles();
}

void Mesh::FindNeighborTriangles()
{
	for(unsigned int i=0; i< m_numFaces; i++)
	{
		Triangle * pTris = &m_pFaces[i];
		int vfist, vsecond;

		for(int vid=0; vid<3; vid++)
		{
			vfist = (vid+1) % 3;
			vsecond = (vid+2) % 3;

			Vertex * pVert_first = pTris->v(vfist, m_pVertices);
			int Neighbor_first = pVert_first->NbPolygonNeighbor();
			
			for(int k=0; k<Neighbor_first; k++)
			{
				Triangle * pNFaces = pVert_first->GetPolygonNeighbor(k);
				if(pNFaces[k].m_v != pTris[i].m_v)
					pNFaces[k].m_flag++;
				else
					continue;
			}

			Vertex * pVert_second = pTris->v(vsecond, m_pVertices);
			int Neighbor_second = pVert_second->NbPolygonNeighbor();
			
			for(int k=0; k<Neighbor_second; k++)
			{
				Triangle * pNFaces = pVert_second->GetPolygonNeighbor(k);
				if(pNFaces[k].m_v != pTris[i].m_v)
					pNFaces[k].m_flag++;
				else
					continue;
				
				char flagTest = pNFaces[k].m_flag % 2;
				if(flagTest == 0)
					pTris->m_uiNidx[vid] = pNFaces[k].m_uiMe;
					//pTris->m_pNeighborT[vid] = &pNFaces[k];
					//neighborT_addr[i].neighborT[vid] = pNFaces[k];		
			}

			for(int k=0; k<Neighbor_first; k++)
			{
				Triangle * pNFaces = pVert_first->GetPolygonNeighbor(k);
				pNFaces[k].m_flag = 0;
			}
			for(int k=0; k<Neighbor_second; k++)
			{
				Triangle * pNFaces = pVert_second->GetPolygonNeighbor(k);
				pNFaces[k].m_flag = 0;
			}
		}
	}
	//VertexAdjacency();
}
Vector3  Mesh:: VertexAdjacency(Triangle t, unsigned int ui)
{
	float Nx=0;
	float Ny=0;
	float Nz=0;

	Vector3 v3VertNormal;
	Vertex * pVert = t.v(ui, m_pVertices);
	
	int NbPolygonNeighbor = pVert->NbPolygonNeighbor();
	//printf(">>>>>>>>>>>>>> %d \n", NbPolygonNeighbor);

	for(int k=0; k<NbPolygonNeighbor; k++)
	{
		Triangle * pNFaces = pVert->GetPolygonNeighbor(k);
		Nx += pNFaces->m_normals.x;
		Ny += pNFaces->m_normals.y;
		Nz += pNFaces->m_normals.z;
	}

	///////// A Triangle's one vertex normal x,y,z of three vertices
	pVert->nx = Nx /(float)NbPolygonNeighbor;
	pVert->ny = Ny /(float)NbPolygonNeighbor;
	pVert->nz = Nz /(float)NbPolygonNeighbor;

	v3VertNormal.x = pVert->nx;
	v3VertNormal.y = pVert->ny;
	v3VertNormal.z = pVert->nz;


	return v3VertNormal;
}
