#ifndef ___BOUNDINGBOX___HHH___
#define ___BOUNDINGBOX___HHH___

#include <float.h>
#include <vector>
#include <Windows.h>
#include "Vertex.h"
#include "Triangle.h"
#include "Vector3.h"
#include "HScommon.h"

class BoundingBox
{
public:
	//Vector3 m_vMax, m_vMin;
	Vertex m_vMax, m_vMin;
	
	//BoundingBox(Vector3 * pVerts, int numVerts);
	BoundingBox(Vertex * pVerts, int numVerts);
	BoundingBox();
	~BoundingBox();
	
	//Vector3 makeBoundingBox(Vector3 * pVerts, int numVerts);
	Vector3 makeBoundingBox(Vertex * pVerts, int numVerts);
	
	//Vector3 * getBBgrid(Vertex * pVerts, Triangle * pTris, int numFaces);
	Vector3 getBBcentroid(void);
	
	bool isInside(Vector3 p);
	Vector3 m_v3Result;
	Vector3 * m_pv3Grid;
	Vector3 * m_pDist;
	
	Vector3 m_v3Length;
	int m_quadCount;
	Vector3 m_quadSize;

	void CopyBB(BoundingBox BB);
	void CopyBB(BoundingBox *pBB);
};
#endif // !___BOUNDINGBOX___HHH___