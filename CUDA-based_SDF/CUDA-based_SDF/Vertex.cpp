#include "Vertex.h"

void Vertex::AddNeighbor(Triangle * pTris)
{
	m_vpFaces.push_back(pTris);
}
/*
void Vertex::AddNeighborEdges(Triangle * pTris)
{
	m_vpFacesEdges.push_back(pTris);
}
*/
int Vertex::NbPolygonNeighbor()
{
	return m_vpFaces.size();
}

Triangle * Vertex::GetPolygonNeighbor(int i)
{
	return m_vpFaces.at(i);
}