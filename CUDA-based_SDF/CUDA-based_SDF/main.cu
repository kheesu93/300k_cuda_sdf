#include <cuda.h>
#include <cuda_runtime.h>
#include <cub/cub.cuh>
#include <stdio.h>
#include <vector>
#include <math.h>
using namespace std;
#include "Mesh.h"
#include "BoundingBox.h"
#include "Stopwatch.h"

#define WARP_SIZE		32
#define PI				3.14159265

Mesh		*	g_ptriMesh;
BoundingBox *	g_pBB;

struct CustomMin
{
    template <typename T>
    __device__ __forceinline__
    T operator()(const T &a, const T &b) const {
        return (b < a) ? b : a;
    }
};

struct DistanceInfo
{
	float 		distance;
	char  		region;
	unsigned int Tidx;
	
	__device__ friend bool operator<(const DistanceInfo& l, const DistanceInfo& r)
    {
		return (l.distance < r.distance);
	}
	
	__device__ friend bool operator>(const DistanceInfo& l, const DistanceInfo& r)
    {
		return (l.distance > r.distance);
	}
	__device__ friend bool operator==(const DistanceInfo& l, const DistanceInfo& r)
    {
		return (l.distance == r.distance);
	}

	/*
	bool operator < (const DistanceInfo& dinfo)
	{
		return (this->distance < dinfo.distance);
	}
	bool operator > (const DistanceInfo& dinfo)
	{
		return (this->distance > dinfo.distance);
	}
	
	bool operator == (const DistanceInfo& dinfo)
	{
		return (this->distance == dinfo.distance);
	}
	*/
};

__device__ DistanceInfo Distance(Vector3 Q, Triangle T)
{
	DistanceInfo	result;
	int				region = -1;
	Vector3			P = Q;

	Vector3			T0 = T.m_v[0];
	Vector3			T1 = T.m_v[1];
	Vector3			T2 = T.m_v[2];

	Vector3			kDiff = T0 - P;
	//MgcVector3 kDiff = rkTri.Origin() - rkPoint;

	float fA00 =  (T1.x - T0.x) * (T1.x - T0.x) + (T1.y -  T0.y) * (T1.y - T0.y) + (T1.z - T0.z) * (T1.z - T0.z);
	float fA01 = (T1.x - T0.x) * (T2.x - T0.x) + (T1.y - T0.y) * (T2.y - T0.y) + (T1.z - T0.z) * (T2.z - T0.z);
	float fA11 = (T2.x - T0.x) * (T2.x - T0.x) + (T2.y - T0.y) * (T2.y - T0.y) + (T2.z - T0.z) * (T2.z - T0.z);
	float fB0 = kDiff.x * (T1.x -  T0.x) + kDiff.y * (T1.y -  T0.y) + kDiff.z * (T1.z -  T0.z);
	float fB1 = kDiff.x * (T2.x -  T0.x) + kDiff.y * (T2.y -  T0.y) + kDiff.z * (T2.z -  T0.z);
	float fC = kDiff.x * kDiff.x + kDiff.y * kDiff.y + kDiff.z * kDiff.z;

	float fDet = fabs(fA00 * fA11 - fA01 * fA01);
	float fS = fA01 * fB1 - fA11 * fB0;
	float fT = fA01 * fB0 - fA00 * fB1;
	float fSqrDist;

	if ( fS + fT <= fDet )
	{
		if ( fS < 0.0 )
		{	          
			if ( fT < 0.0 )  // region 4
			{
				region = 4;
				if ( fB0 < 0.0 )
				{
					fT = 0.0;
					if ( -fB0 >= fA00 )
					{
						fS = 1.0;
						fSqrDist = fA00+2.0*fB0+fC;
					}
					else
					{
						fS = -fB0/fA00;
						fSqrDist = fB0*fS+fC;
					}
				}
				else
				{
					fS = 0.0;
					if ( fB1 >= 0.0 )
					{
						fT = 0.0;
						fSqrDist = fC;
					}
					else if ( -fB1 >= fA11 )
					{
						fT = 1.0;
						fSqrDist = fA11+2.0*fB1+fC;			    
					}
					else
					{
						fT = -fB1/fA11;
						fSqrDist = fB1*fT+fC;
					}
				}
			}
			else  // region 3
			{
				region = 3;
				fS = 0.0;
				if ( fB1 >= 0.0 )
				{
					fT = 0.0;
					fSqrDist = fC;
				}
				else if ( -fB1 >= fA11 )
				{
					fT = 1;
					fSqrDist = fA11+2.0*fB1+fC;
				}
				else
				{
					fT = -fB1/fA11;
					fSqrDist = fB1*fT+fC;
				}
			}
		}
		else if ( fT < 0.0 )  // region 5
		{
			region = 5;

			fT = 0.0;
			if ( fB0 >= 0.0 )
			{
				fS = 0.0;
				fSqrDist = fC;
			}
			else if ( -fB0 >= fA00 )
			{
				fS = 1.0;
				fSqrDist = fA00+2.0*fB0+fC;
			}
			else
			{
				fS = -fB0/fA00;
				fSqrDist = fB0*fS+fC;
			}
		}
		else  // region 0
		{
			region = 0;
			// minimum at interior point
			float fInvDet = 1.0/fDet;
			fS *= fInvDet;
			fT *= fInvDet;
			fSqrDist = fS*(fA00*fS+fA01*fT+2.0*fB0) +
				fT*(fA01*fS+fA11*fT+2.0*fB1)+fC;
		}
	}
	else
	{
		float fTmp0, fTmp1, fNumer, fDenom;

		if ( fS < 0.0 )  // region 2
		{
			region = 2;
			fTmp0 = fA01 + fB0;
			fTmp1 = fA11 + fB1;
			if ( fTmp1 > fTmp0 )
			{
				fNumer = fTmp1 - fTmp0;
				fDenom = fA00-2.0*fA01+fA11;
				if ( fNumer >= fDenom )
				{
					fS = 1.0;
					fT = 0.0;
					fSqrDist = fA00+2.0*fB0+fC;
				}
				else
				{
					fS = fNumer/fDenom;
					fT = 1.0 - fS;
					fSqrDist = fS*(fA00*fS+fA01*fT+2.0*fB0) +
						fT*(fA01*fS+fA11*fT+2.0*fB1)+fC;
				}
			}
			else
			{
				fS = 0.0;
				if ( fTmp1 <= 0.0 )
				{
					fT = 1.0;
					fSqrDist = fA11+2.0*fB1+fC;
				}
				else if ( fB1 >= 0.0 )
				{
					fT = 0.0;
					fSqrDist = fC;
				}
				else
				{
					fT = -fB1/fA11;
					fSqrDist = fB1*fT+fC;
				}
			}
		}
		else if ( fT < 0.0 )  // region 6
		{
			region = 6;
			fTmp0 = fA01 + fB1;
			fTmp1 = fA00 + fB0;
			if ( fTmp1 > fTmp0 )
			{
				fNumer = fTmp1 - fTmp0;
				fDenom = fA00-2.0*fA01+fA11;
				if ( fNumer >= fDenom )
				{
					fT = 1.0;
					fS = 0.0;
					fSqrDist = fA11+2.0*fB1+fC;
				}
				else
				{
					fT = fNumer/fDenom;
					fS = 1.0 - fT;
					fSqrDist = fS*(fA00*fS+fA01*fT+2.0*fB0) +
						fT*(fA01*fS+fA11*fT+2.0*fB1)+fC;
				}
			}
			else
			{
				fT = 0.0;
				if ( fTmp1 <= 0.0 )
				{
					fS = 1.0;
					fSqrDist = fA00+2.0*fB0+fC;
				}
				else if ( fB0 >= 0.0 )
				{
					fS = 0.0;
					fSqrDist = fC;
				}
				else
				{
					fS = -fB0/fA00;
					fSqrDist = fB0*fS+fC;
				}
			}
		}
		else  // region 1
		{
			region = 1;

			fNumer = fA11 + fB1 - fA01 - fB0;
			if ( fNumer <= 0.0 )
			{
				fS = 0.0;
				fT = 1.0;
				fSqrDist = fA11+2.0*fB1+fC;
			}
			else
			{
				fDenom = fA00-2.0*fA01+fA11;
				if ( fNumer >= fDenom )
				{
					fS = 1.0;
					fT = 0.0;
					fSqrDist = fA00+2.0*fB0+fC;
				}
				else
				{
					fS = fNumer/fDenom;
					fT = 1.0 - fS;
					fSqrDist = fS*(fA00*fS+fA01*fT+2.0*fB0) +
						fT*(fA01*fS+fA11*fT+2.0*fB1)+fC;
				}
			}
		}
	}
	
	result.distance = sqrt(fSqrDist);	
	result.region = (char)region;
	result.Tidx = T.m_uiMe;
	return result;
}

__global__	void CUDA_based_SDF(Vector3  d_query, Triangle * d_inputTris, unsigned int num_triangles, DistanceInfo * Distout)
{
	const unsigned int tid = blockIdx.x * blockDim.x + threadIdx.x;
	if(num_triangles <= tid)
		return;

	Triangle myTriangle = d_inputTris[tid];
	DistanceInfo DistInfo = Distance(d_query, myTriangle);
	Distout[tid] = DistInfo;
}

int main()
{	
	CStopWatch timer;

	std::string git_dir(getenv("GITDIR"));
	git_dir += "/mesh/sphere.ply";
	
	g_ptriMesh = new Mesh();
	g_ptriMesh->meshRead(git_dir);
	g_pBB =  new BoundingBox(g_ptriMesh->m_pVertices, g_ptriMesh->m_numVerts); 

	unsigned int				num_queries;
	unsigned int				num_triangles = g_ptriMesh->m_numFaces;
	unsigned int				num_vertices = g_ptriMesh->m_numVerts;
	float						x0;
	float						y0;
	float						z0;

	unsigned int				blocksize = 64;
	dim3						block;
	dim3						grid;
	
	CustomMin					min_op;
	DistanceInfo				init;
	init.distance = FLT_MAX;
	void			*			d_temp_storage = NULL;
	size_t						temp_storage_bytes = 0;

	Triangle		*			tpFaces = g_ptriMesh->m_pFaces;
	Triangle		*			d_faces;	

	DistanceInfo	*			d_Dist;
	DistanceInfo	*			h_Dist = new DistanceInfo[num_triangles];
	DistanceInfo	*			d_outDist;
	DistanceInfo				h_outDist;

	cudaMalloc(&d_faces, sizeof(Triangle) * num_triangles);
	cudaMalloc(&d_Dist, sizeof(DistanceInfo) * num_triangles);
	cudaMalloc(&d_outDist, sizeof(DistanceInfo));
	cudaMemcpy(d_faces, tpFaces, sizeof(Triangle)*num_triangles, cudaMemcpyHostToDevice);

	block.x = blocksize;
	grid.x = (num_triangles + block.x - 1) / block.x;
	
	std::string git_dir2(getenv("GITDIR"));
	git_dir2 += "/mesh/Query_samples2.txt";
	//git_dir2 += "/mesh/query_points00.txt";

	timer.Start();

	FILE * fp = fopen(git_dir2.c_str(),"r");
	fscanf(fp, "%d\n", &num_queries);

	Vector3	 		*			v3pQuery = new Vector3[num_queries];

	for(unsigned int i = 0; i < num_queries; i++)
	{	
		fscanf(fp, "%f\t%f\t%f\n", &x0, &y0, &z0);

		v3pQuery[i].x = x0;
		v3pQuery[i].y = y0;
		v3pQuery[i].z = z0;
	}
	fclose(fp);
	
	printf("\n..fscanf End..");
	timer.Stop();
	timer.PrintAccumTime();

	//FILE * fp2 = fopen("QueryToDistance_10.txt","w");
	FILE * fp2 = fopen("Query_Nearest_SDF.txt","w");
	//fprintf(fp2, "idx\t\tdist\t\tregion\t\tTidx\n");
	fprintf(fp2, "Qidx\tNidx\tSigned_Distance\n");
	
	for(unsigned int j = 0; j < num_queries; j++)
	{
		
		CUDA_based_SDF<<<grid, block>>>(v3pQuery[j], d_faces, num_triangles, d_Dist);
		//cudaMemcpy(h_Dist, d_Dist, sizeof(DistanceInfo)*num_triangles, cudaMemcpyDeviceToHost);
		//for(int k=0; k< num_triangles; k++)
		//	fprintf(fp2, "%u\t\t%f\t\t%d\t\t%d\n", j, h_Dist[k].distance, (int)h_Dist[k].region, h_Dist[k].Tidx);

		cub::DeviceReduce::Reduce(d_temp_storage, temp_storage_bytes, d_Dist, d_outDist, num_triangles, min_op, init);
		cudaMalloc(&d_temp_storage, temp_storage_bytes);
		cub::DeviceReduce::Reduce(d_temp_storage, temp_storage_bytes, d_Dist, d_outDist, num_triangles, min_op, init);

		cudaMemcpy(&h_outDist, d_outDist, sizeof(DistanceInfo), cudaMemcpyDeviceToHost);
		//fprintf(fp2, "%u\t\t%f\t\t%d\t\t%d\n", j, h_outDist.distance, (int)h_outDist.region, h_outDist.Tidx);
		
		float sign = 100.0f;
		
		if(h_outDist.region == 0)
		{
			Vector3	qp;
			qp.x = tpFaces[h_outDist.Tidx].m_v[0].x - v3pQuery[j].x;
			qp.y = tpFaces[h_outDist.Tidx].m_v[0].y - v3pQuery[j].y;
			qp.z = tpFaces[h_outDist.Tidx].m_v[0].z - v3pQuery[j].z;

			float abs_qp = sqrt(qp.x*qp.x + qp.y*qp.y + qp.z*qp.z);
			float cos_theta = (qp.x * tpFaces[h_outDist.Tidx].m_normals.x) + (qp.y * tpFaces[h_outDist.Tidx].m_normals.y) + (qp.z * tpFaces[h_outDist.Tidx].m_normals.z) / abs_qp;
			float theta = acos(cos_theta * PI / 180.0);

			if(theta>= -90.0 && theta <= 90.0)
				sign = 1.0;
			else
				sign = -1.0;
		}else if(h_outDist.region == 1 || h_outDist.region == 3 || h_outDist.region == 5)
		{
			Vector3 edNormal;
			if(h_outDist.region == 1){
				edNormal.x = (tpFaces[h_outDist.Tidx].m_normals.x + tpFaces[tpFaces[h_outDist.Tidx].m_uiNidx[0]].m_normals.x)/2.0;
				edNormal.y = (tpFaces[h_outDist.Tidx].m_normals.y + tpFaces[tpFaces[h_outDist.Tidx].m_uiNidx[0]].m_normals.y)/2.0;
				edNormal.z = (tpFaces[h_outDist.Tidx].m_normals.z + tpFaces[tpFaces[h_outDist.Tidx].m_uiNidx[0]].m_normals.z)/2.0;
			}else if(h_outDist.region == 3){
				edNormal.x = (tpFaces[h_outDist.Tidx].m_normals.x + tpFaces[tpFaces[h_outDist.Tidx].m_uiNidx[1]].m_normals.x)/2.0;
				edNormal.y = (tpFaces[h_outDist.Tidx].m_normals.y + tpFaces[tpFaces[h_outDist.Tidx].m_uiNidx[1]].m_normals.y)/2.0;
				edNormal.z = (tpFaces[h_outDist.Tidx].m_normals.z + tpFaces[tpFaces[h_outDist.Tidx].m_uiNidx[1]].m_normals.z)/2.0;
			}else{
				edNormal.x = (tpFaces[h_outDist.Tidx].m_normals.x + tpFaces[tpFaces[h_outDist.Tidx].m_uiNidx[2]].m_normals.x)/2.0;
				edNormal.y = (tpFaces[h_outDist.Tidx].m_normals.y + tpFaces[tpFaces[h_outDist.Tidx].m_uiNidx[2]].m_normals.y)/2.0;
				edNormal.z = (tpFaces[h_outDist.Tidx].m_normals.z + tpFaces[tpFaces[h_outDist.Tidx].m_uiNidx[2]].m_normals.z)/2.0;
			}
			
			Vector3	qp;
			qp.x = tpFaces[h_outDist.Tidx].m_v[0].x - v3pQuery[j].x;
			qp.y = tpFaces[h_outDist.Tidx].m_v[0].y - v3pQuery[j].y;
			qp.z = tpFaces[h_outDist.Tidx].m_v[0].z - v3pQuery[j].z;

			float abs_qp = sqrt(qp.x*qp.x + qp.y*qp.y + qp.z*qp.z);
			float cos_theta = (qp.x * edNormal.x) + (qp.y * edNormal.y) + (qp.z * edNormal.z) / abs_qp;
			float theta = acos(cos_theta * PI / 180.0);

			if(theta>= -90.0 && theta <= 90.0)
				sign = 1;
			else
				sign = -1;
		}else if(h_outDist.region == 2 || h_outDist.region == 4 || h_outDist.region == 6)
		{
			Vector3 vertNormal;
			
			if(h_outDist.region == 2)
				vertNormal = g_ptriMesh->VertexAdjacency(tpFaces[h_outDist.Tidx], 2);	// Triangle & m_v[0]의 0을 넘겨준것....
			else if(h_outDist.region == 4)
				vertNormal = g_ptriMesh->VertexAdjacency(tpFaces[h_outDist.Tidx], 0);
			else
				vertNormal = g_ptriMesh->VertexAdjacency(tpFaces[h_outDist.Tidx], 1);

			Vector3	qp;
			qp.x = tpFaces[h_outDist.Tidx].m_v[0].x - v3pQuery[j].x;
			qp.y = tpFaces[h_outDist.Tidx].m_v[0].y - v3pQuery[j].y;
			qp.z = tpFaces[h_outDist.Tidx].m_v[0].z - v3pQuery[j].z;

			float abs_qp = sqrt(qp.x*qp.x + qp.y*qp.y + qp.z*qp.z);
			float cos_theta = (qp.x * vertNormal.x) + (qp.y * vertNormal.y) + (qp.z * vertNormal.z) / abs_qp;
			float theta = acos(cos_theta * PI / 180.0);

			if(theta>= -90.0 && theta <= 90.0)
				sign = 1;
			else
				sign = -1;
		}

		fprintf(fp2, "%u\t\t%u\t\t%f\n", j, h_outDist.Tidx, sign*abs(h_outDist.distance));
		
	}
	fclose(fp2);
	printf("\n..Reduce End..");
	timer.Stop();
	timer.PrintAccumTime();
	
	delete [] tpFaces;
	delete [] v3pQuery;
	delete [] h_Dist;

	cudaFree(d_faces);
	cudaFree(d_temp_storage);
	cudaFree(d_Dist);
	cudaFree(d_outDist);
}