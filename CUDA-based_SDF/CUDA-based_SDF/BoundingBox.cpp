#include <stdio.h>
#include "BoundingBox.h"

//BoundingBox::BoundingBox(Vector3 * pVerts, int numVerts)
BoundingBox::BoundingBox(Vertex * pVerts, int numVerts)
{
	if ((pVerts) == NULL)
	{ 
		printf("\n Error! BoundingBox::BoundingBox(Vertex * pVerts, int numVerts) : null point received!");
		return;
	}
	makeBoundingBox(pVerts, numVerts);
}

BoundingBox::BoundingBox()
{
	m_pv3Grid = NULL;
}

BoundingBox::~BoundingBox()
{
	SAFEDELETEARRAY(m_pv3Grid);
}

void BoundingBox::CopyBB(BoundingBox BB)
{
	m_vMax = BB.m_vMax;
	m_vMin = BB.m_vMin;
}

void BoundingBox::CopyBB(BoundingBox *pBB)
{
	if (!pBB)
		return;
	CopyBB(*pBB);
}

//Vector3 BoundingBox:: makeBoundingBox(Vector3 * pVerts, int numVerts)
Vector3 BoundingBox:: makeBoundingBox(Vertex * pVerts, int numVerts)
{
	m_vMax.x = -FLT_MAX;
	m_vMax.y = -FLT_MAX;
	m_vMax.z = -FLT_MAX;

	m_vMin.x = FLT_MAX;
	m_vMin.y = FLT_MAX;
	m_vMin.z = FLT_MAX;

	for (int i=0; i<numVerts; i++)
	{
		///////////////	X_MAX, X_MIN
		if (pVerts[i].x > m_vMax.x)
			m_vMax.x = pVerts[i].x;
		if (pVerts[i].x < m_vMin.x)
			m_vMin.x = pVerts[i].x;

		///////////////	Y_MAX, Y_MIN
		if (pVerts[i].y > m_vMax.y)
			m_vMax.y = pVerts[i].y;
		if (pVerts[i].y < m_vMin.y)
			m_vMin.y = pVerts[i].y;

		///////////////	Z_MAX, Z_MIN
		if (pVerts[i].z > m_vMax.z)
			m_vMax.z = pVerts[i].z;
		if (pVerts[i].z < m_vMin.z)
			m_vMin.z = pVerts[i].z;
	}

	m_vMax.x = m_vMax.x * 2.0f;
	m_vMax.y = m_vMax.y * 2.0f;
	m_vMax.z = m_vMax.z * 2.0f;

	m_vMin.x = m_vMin.x * 2.0f;
	m_vMin.y = m_vMin.y * 2.0f;
	m_vMin.z = m_vMin.z * 2.0f;

	m_v3Length.x = m_vMax.x - m_vMin.x;
	m_v3Length.y = m_vMax.y - m_vMin.y;
	m_v3Length.z = m_vMax.z - m_vMin.z;

	//printf("<makeBoundingBox>\nMax(%f, %f, %f)\nMin(%f, %f, %f)\n", m_vMax.x, m_vMax.y, m_vMax.z, m_vMin.x, m_vMin.y, m_vMin.z);

	return m_v3Length;
}

Vector3 BoundingBox:: getBBcentroid(void)
{
	//Vector3 m_v3Result;
	//m_v3Result_T = (0.0f, 0.0f, 0.0f);
	m_v3Result.x = 0.0f;
	m_v3Result.y = 0.0f;
	m_v3Result.z = 0.0f;

	m_v3Result.x = (m_vMax.x + m_vMin.x) / 2.0f;
	m_v3Result.y = (m_vMax.y + m_vMin.y) / 2.0f;
	m_v3Result.z = (m_vMax.z + m_vMin.z) / 2.0f;

	printf("\nBBcentroid m_v3Result(%f, %f, %f)\n",m_v3Result.x, m_v3Result.y, m_v3Result.z);

	return m_v3Result;
}

/*
Vector3 * BoundingBox:: getBBgrid(Vertex * pVerts, Triangle * pTris, int numFaces)
{
	m_quadCount = 2;
	m_v3Length.x = m_vMax.x - m_vMin.x;
	m_v3Length.y = m_vMax.y - m_vMin.y;
	m_v3Length.z = m_vMax.z - m_vMin.z;

	//float lmax = max(max(m_v3Length.x, m_v3Length.y), m_v3Length.z);
	
	m_quadSize.x = m_v3Length.x / m_quadCount;
	m_quadSize.y = m_v3Length.y / m_quadCount;
	m_quadSize.z = m_v3Length.z / m_quadCount;

	//printf("quadSize(%f, %f, %f)\n\n",m_quadSize.x, m_quadSize.y, m_quadSize.z);

	int pointCount = (m_quadCount + 1) * (m_quadCount + 1) * (m_quadCount + 1);
	m_pv3Grid = new Vector3[pointCount];
	m_pDist = new Vector3[pointCount];
	float x0, y0, z0;
	int lin_addr;
	FILE * fp = fopen("Grid.txt","w");
	fprintf(fp, "%d\n", numFaces * pointCount);
	for(int i = 0; i < m_quadCount + 1; ++i)
	{
		z0= m_vMin.z + (float) i * m_quadSize.z;
		//m_pv3Grid->z = m_vMin.z + i * m_quadSize.z;

		for(int j = 0; j < m_quadCount + 1; ++j)
		{
			y0 = m_vMin.y + j * m_quadSize.y;
			//m_pv3Grid->y = m_vMin.y + j * m_quadSize.y;

			for(int k = 0; k < m_quadCount + 1; ++k)
			{	
				x0 = m_vMin.x + k * m_quadSize.x;
				lin_addr = k+ ( m_quadCount + 1) * j + (m_quadCount + 1)*(m_quadCount + 1)*i;
				//m_pv3Grid->x = m_vMin.x + k * m_quadSize.x;
				m_pv3Grid[lin_addr].x = x0;
				m_pv3Grid[lin_addr].y = y0;
				m_pv3Grid[lin_addr].z = z0;
				
				for(int q = 0; q < numFaces; q++)
				{
					double mx = m_pv3Grid[k].x - pVerts[pTris[q].m_v[0]].x;
					double my = m_pv3Grid[j].y - pVerts[pTris[q].m_v[0]].y;
					double mz = m_pv3Grid[i].z - pVerts[pTris[q].m_v[0]].z;
					m_pDist->x = m_pv3Grid[k].x - pVerts[pTris[q].m_v[0]].x;
					m_pDist->y = m_pv3Grid[j].y - pVerts[pTris[q].m_v[0]].y;
					m_pDist->z = m_pv3Grid[i].z - pVerts[pTris[q].m_v[0]].z;

					//double m_pDistance = sqrt((double)(mx*mx) + (my*my) + (mz*mz));
					//double m_pDistance = sqrt((double)(m_pDist->x*m_pDist->x) + (m_pDist->y*m_pDist->y) + (m_pDist->z*m_pDist->z));
					m_pDist->x = m_pv3Grid->x - pTris[q].getTcentroid().x;
					m_pDist->y = m_pv3Grid->y - pTris[q].getTcentroid().y;
					m_pDist->z = m_pv3Grid->z - pTris[q].getTcentroid().z;
					double m_pDistance = sqrt((double)(m_pDist->x*m_pDist->x) + (m_pDist->y*m_pDist->y) + (m_pDist->z*m_pDist->z));
					
					
					//fprintf(fp, "%f\n", m_pDistance);
					
				}
			}
		}
	}

	fclose(fp);
	return m_pv3Grid;
	//return m_pDistance;
}
*/
bool BoundingBox:: isInside(Vector3 p)
{
	if ((m_vMin.x <= p.x && p.x <= m_vMax.x) && (m_vMin.y <= p.y && p.y <= m_vMax.y) && (m_vMin.z <= p.z && p.z <= m_vMax.z))
		return true;
	else
		return false;
}