#include <cuda.h>
#include <cuda_runtime.h>

#ifndef ______VECTOR3___HHH_____
#define ______VECTOR3___HHH_____

class Vector3
{
public:

	float			x, y, z;
	/* 
	//////////////////////////////////////////// Combine Error : Vertex.h  and Vector3.h
	float			nx, ny, nz;

	vector<Triangle*> m_vpFaces;
	vector<Triangle*> m_vpFacesEdges;

	void AddNeighbor(Triangle * pTris);
	void AddNeighborEdges(Triangle * pTris);
	int NbPolygonNeighbor();
	Triangle * GetPolygonNeighbor(int i);

	unsigned char	r, g, b, a;
	Vector3& operator=(const Vector3& other) // copy assignment
	{
		x = other.x;
		y = other.y;
		z = other.z;
		r = other.r;
		g = other.g;
		b = other.b;
		a = other.a;
		return *this;
	}
	*/
	
	__host__ __device__ Vector3& operator-(const Vector3& a)
	{
		x -= a.x;
		y -= a.y;
		z -= a.z;
		return *this;
	}

	__host__ __device__ Vector3& operator/(const Vector3& a)
	{
		x /= a.x;
		y /= a.y;
		z /= a.z;
		return *this;
	}

	__host__ __device__ Vector3& operator*(const Vector3& a)
	{
		x *= a.x;
		y *= a.y;
		z *= a.z;
		return *this;
	}
};
#endif // !______VECTOR3___HHH_____
