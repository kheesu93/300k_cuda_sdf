#ifndef ___VERTEX___HHH___
#define ___VERTEX___HHH___

#include <vector>
#include "Array3d.h"

using namespace std;
class Triangle;

class Vertex
{
public:
	float			x, y, z;
	unsigned char	r, g, b, a;
	float			nx, ny, nz;

	vector<Triangle*> m_vpFaces;
	void AddNeighbor(Triangle * pTris);	
	int NbPolygonNeighbor();
	Triangle * GetPolygonNeighbor(int i);

	Vertex& operator=(const Vertex& other) // copy assignment
	{
		x = other.x;
		y = other.y;
		z = other.z;
		r = other.r;
		g = other.g;
		b = other.b;
		a = other.a;
		return *this;
	}
};

#endif // !___VERTEX___HHH___