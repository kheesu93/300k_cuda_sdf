#include <cuda.h>
#include <cuda_runtime.h>
#include <math.h>
#include "Vector3.h"
#include "Triangle.h"

struct DistanceInfo
{
	float distance;
	char region;
};

__device__ DistanceInfo Distance(Vector3 Q, Triangle * T)
{
	DistanceInfo result;
	int region = -1;
	Vector3	P = Q;

	Vector3 T0 = T->m_v[0];
	Vector3 T1 = T->m_v[1];
	Vector3 T2 = T->m_v[2];



	//MgcVector3 kDiff = rkTri.Origin() - rkPoint;
	Vector3 kDiff = T0 - P;


	float fA00 =  (T1.x -  T0.x)*(T1.x -  T0.x) + (T1.y -  T0.y)*(T1.y -  T0.y) + (T1.z -  T0.z)*(T1.z -  T0.z);
	float fA01 = (T1.x -  T0.x)*(T2.x -  T0.x) + (T1.y -  T0.y)*(T2.y -  T0.y) + (T1.z -  T0.z)*(T2.z -  T0.z);
	float fA11 =(T2.x -  T0.x)*(T2.x -  T0.x) + (T2.y -  T0.y)*(T2.y -  T0.y) + (T2.z -  T0.z)*(T2.z -  T0.z);
	float fB0 =kDiff.x*(T1.x -  T0.x) +kDiff.y*(T1.y -  T0.y) + kDiff.z*(T1.z -  T0.z);
	float fB1 =kDiff.x*(T2.x -  T0.x) +kDiff.y*(T2.y -  T0.y) + kDiff.z*(T2.z -  T0.z);
	float fC = kDiff.x*kDiff.x+kDiff.y*kDiff.y+kDiff.z*kDiff.z;

	float fDet = fabs(fA00*fA11-fA01*fA01);
	float fS = fA01*fB1-fA11*fB0;
	float fT = fA01*fB0-fA00*fB1;
	float fSqrDist;

	if ( fS + fT <= fDet )
	{
		if ( fS < 0.0 )
		{	          
			if ( fT < 0.0 )  // region 4
			{
				region = 4;
				if ( fB0 < 0.0 )
				{
					fT = 0.0;
					if ( -fB0 >= fA00 )
					{
						fS = 1.0;
						fSqrDist = fA00+2.0*fB0+fC;
					}
					else
					{
						fS = -fB0/fA00;
						fSqrDist = fB0*fS+fC;
					}
				}
				else
				{
					fS = 0.0;
					if ( fB1 >= 0.0 )
					{
						fT = 0.0;
						fSqrDist = fC;
					}
					else if ( -fB1 >= fA11 )
					{
						fT = 1.0;
						fSqrDist = fA11+2.0*fB1+fC;			    
					}
					else
					{
						fT = -fB1/fA11;
						fSqrDist = fB1*fT+fC;
					}
				}
			}
			else  // region 3
			{
				region = 3;
				fS = 0.0;
				if ( fB1 >= 0.0 )
				{
					fT = 0.0;
					fSqrDist = fC;
				}
				else if ( -fB1 >= fA11 )
				{
					fT = 1;
					fSqrDist = fA11+2.0*fB1+fC;
				}
				else
				{
					fT = -fB1/fA11;
					fSqrDist = fB1*fT+fC;
				}
			}
		}
		else if ( fT < 0.0 )  // region 5
		{
			region = 5;
			fT = 0.0;
			if ( fB0 >= 0.0 )
			{
				fS = 0.0;
				fSqrDist = fC;
			}
			else if ( -fB0 >= fA00 )
			{
				fS = 1.0;
				fSqrDist = fA00+2.0*fB0+fC;
			}
			else
			{
				fS = -fB0/fA00;
				fSqrDist = fB0*fS+fC;
			}
		}
		else  // region 0
		{
			region = 0;
			// minimum at interior point
			float fInvDet = 1.0/fDet;
			fS *= fInvDet;
			fT *= fInvDet;
			fSqrDist = fS*(fA00*fS+fA01*fT+2.0*fB0) +
				fT*(fA01*fS+fA11*fT+2.0*fB1)+fC;
		}
	}
	else
	{
		float fTmp0, fTmp1, fNumer, fDenom;

		if ( fS < 0.0 )  // region 2
		{
			region = 2;
			fTmp0 = fA01 + fB0;
			fTmp1 = fA11 + fB1;
			if ( fTmp1 > fTmp0 )
			{
				fNumer = fTmp1 - fTmp0;
				fDenom = fA00-2.0*fA01+fA11;
				if ( fNumer >= fDenom )
				{
					fS = 1.0;
					fT = 0.0;
					fSqrDist = fA00+2.0*fB0+fC;
				}
				else
				{
					fS = fNumer/fDenom;
					fT = 1.0 - fS;
					fSqrDist = fS*(fA00*fS+fA01*fT+2.0*fB0) +
						fT*(fA01*fS+fA11*fT+2.0*fB1)+fC;
				}
			}
			else
			{
				fS = 0.0;
				if ( fTmp1 <= 0.0 )
				{
					fT = 1.0;
					fSqrDist = fA11+2.0*fB1+fC;
				}
				else if ( fB1 >= 0.0 )
				{
					fT = 0.0;
					fSqrDist = fC;
				}
				else
				{
					fT = -fB1/fA11;
					fSqrDist = fB1*fT+fC;
				}
			}
		}
		else if ( fT < 0.0 )  // region 6
		{
			region = 6;
			fTmp0 = fA01 + fB1;
			fTmp1 = fA00 + fB0;
			if ( fTmp1 > fTmp0 )
			{
				fNumer = fTmp1 - fTmp0;
				fDenom = fA00-2.0*fA01+fA11;
				if ( fNumer >= fDenom )
				{
					fT = 1.0;
					fS = 0.0;
					fSqrDist = fA11+2.0*fB1+fC;
				}
				else
				{
					fT = fNumer/fDenom;
					fS = 1.0 - fT;
					fSqrDist = fS*(fA00*fS+fA01*fT+2.0*fB0) +
						fT*(fA01*fS+fA11*fT+2.0*fB1)+fC;
				}
			}
			else
			{
				fT = 0.0;
				if ( fTmp1 <= 0.0 )
				{
					fS = 1.0;
					fSqrDist = fA00+2.0*fB0+fC;
				}
				else if ( fB0 >= 0.0 )
				{
					fS = 0.0;
					fSqrDist = fC;
				}
				else
				{
					fS = -fB0/fA00;
					fSqrDist = fB0*fS+fC;
				}
			}
		}
		else  // region 1
		{
			region = 1;

			fNumer = fA11 + fB1 - fA01 - fB0;
			if ( fNumer <= 0.0 )
			{
				fS = 0.0;
				fT = 1.0;
				fSqrDist = fA11+2.0*fB1+fC;
			}
			else
			{
				fDenom = fA00-2.0*fA01+fA11;
				if ( fNumer >= fDenom )
				{
					fS = 1.0;
					fT = 0.0;
					fSqrDist = fA00+2.0*fB0+fC;
				}
				else
				{
					fS = fNumer/fDenom;
					fT = 1.0 - fS;
					fSqrDist = fS*(fA00*fS+fA01*fT+2.0*fB0) +
						fT*(fA01*fS+fA11*fT+2.0*fB1)+fC;
				}
			}
		}
	}
	result.distance = sqrt(fSqrDist);	
	result.region = region;

	return result;
}


/*
__device__ DistanceInfo Distance(optix::float3  Q, Triangle T)
{
	DistanceInfo result;
	int region = -1;
	optix::float3	P = Q;

	optix::float3 T0 = T.v[0];
	optix::float3 T1 = T.v[1];
	optix::float3 T2 = T.v[2];



	//MgcVector3 kDiff = rkTri.Origin() - rkPoint;
	optix::float3 kDiff = T0 - P;


	float fA00 =  (T1.x -  T0.x)*(T1.x -  T0.x) + (T1.y -  T0.y)*(T1.y -  T0.y) + (T1.z -  T0.z)*(T1.z -  T0.z);
	float fA01 = (T1.x -  T0.x)*(T2.x -  T0.x) + (T1.y -  T0.y)*(T2.y -  T0.y) + (T1.z -  T0.z)*(T2.z -  T0.z);
	float fA11 =(T2.x -  T0.x)*(T2.x -  T0.x) + (T2.y -  T0.y)*(T2.y -  T0.y) + (T2.z -  T0.z)*(T2.z -  T0.z);
	float fB0 =kDiff.x*(T1.x -  T0.x) +kDiff.y*(T1.y -  T0.y) + kDiff.z*(T1.z -  T0.z);
	float fB1 =kDiff.x*(T2.x -  T0.x) +kDiff.y*(T2.y -  T0.y) + kDiff.z*(T2.z -  T0.z);
	float fC = kDiff.x*kDiff.x+kDiff.y*kDiff.y+kDiff.z*kDiff.z;

	float fDet = fabs(fA00*fA11-fA01*fA01);
	float fS = fA01*fB1-fA11*fB0;
	float fT = fA01*fB0-fA00*fB1;
	float fSqrDist;

	if ( fS + fT <= fDet )
	{
		if ( fS < 0.0 )
		{	          
			if ( fT < 0.0 )  // region 4
			{
				region = 4;
				if ( fB0 < 0.0 )
				{
					fT = 0.0;
					if ( -fB0 >= fA00 )
					{
						fS = 1.0;
						fSqrDist = fA00+2.0*fB0+fC;
					}
					else
					{
						fS = -fB0/fA00;
						fSqrDist = fB0*fS+fC;
					}
				}
				else
				{
					fS = 0.0;
					if ( fB1 >= 0.0 )
					{
						fT = 0.0;
						fSqrDist = fC;
					}
					else if ( -fB1 >= fA11 )
					{
						fT = 1.0;
						fSqrDist = fA11+2.0*fB1+fC;			    
					}
					else
					{
						fT = -fB1/fA11;
						fSqrDist = fB1*fT+fC;
					}
				}
			}
			else  // region 3
			{
				region = 3;
				fS = 0.0;
				if ( fB1 >= 0.0 )
				{
					fT = 0.0;
					fSqrDist = fC;
				}
				else if ( -fB1 >= fA11 )
				{
					fT = 1;
					fSqrDist = fA11+2.0*fB1+fC;
				}
				else
				{
					fT = -fB1/fA11;
					fSqrDist = fB1*fT+fC;
				}
			}
		}
		else if ( fT < 0.0 )  // region 5
		{
			region = 5;
			fT = 0.0;
			if ( fB0 >= 0.0 )
			{
				fS = 0.0;
				fSqrDist = fC;
			}
			else if ( -fB0 >= fA00 )
			{
				fS = 1.0;
				fSqrDist = fA00+2.0*fB0+fC;
			}
			else
			{
				fS = -fB0/fA00;
				fSqrDist = fB0*fS+fC;
			}
		}
		else  // region 0
		{
			region = 0;
			// minimum at interior point
			float fInvDet = 1.0/fDet;
			fS *= fInvDet;
			fT *= fInvDet;
			fSqrDist = fS*(fA00*fS+fA01*fT+2.0*fB0) +
				fT*(fA01*fS+fA11*fT+2.0*fB1)+fC;
		}
	}
	else
	{
		float fTmp0, fTmp1, fNumer, fDenom;

		if ( fS < 0.0 )  // region 2
		{
			region = 2;
			fTmp0 = fA01 + fB0;
			fTmp1 = fA11 + fB1;
			if ( fTmp1 > fTmp0 )
			{
				fNumer = fTmp1 - fTmp0;
				fDenom = fA00-2.0*fA01+fA11;
				if ( fNumer >= fDenom )
				{
					fS = 1.0;
					fT = 0.0;
					fSqrDist = fA00+2.0*fB0+fC;
				}
				else
				{
					fS = fNumer/fDenom;
					fT = 1.0 - fS;
					fSqrDist = fS*(fA00*fS+fA01*fT+2.0*fB0) +
						fT*(fA01*fS+fA11*fT+2.0*fB1)+fC;
				}
			}
			else
			{
				fS = 0.0;
				if ( fTmp1 <= 0.0 )
				{
					fT = 1.0;
					fSqrDist = fA11+2.0*fB1+fC;
				}
				else if ( fB1 >= 0.0 )
				{
					fT = 0.0;
					fSqrDist = fC;
				}
				else
				{
					fT = -fB1/fA11;
					fSqrDist = fB1*fT+fC;
				}
			}
		}
		else if ( fT < 0.0 )  // region 6
		{
			region = 6;
			fTmp0 = fA01 + fB1;
			fTmp1 = fA00 + fB0;
			if ( fTmp1 > fTmp0 )
			{
				fNumer = fTmp1 - fTmp0;
				fDenom = fA00-2.0*fA01+fA11;
				if ( fNumer >= fDenom )
				{
					fT = 1.0;
					fS = 0.0;
					fSqrDist = fA11+2.0*fB1+fC;
				}
				else
				{
					fT = fNumer/fDenom;
					fS = 1.0 - fT;
					fSqrDist = fS*(fA00*fS+fA01*fT+2.0*fB0) +
						fT*(fA01*fS+fA11*fT+2.0*fB1)+fC;
				}
			}
			else
			{
				fT = 0.0;
				if ( fTmp1 <= 0.0 )
				{
					fS = 1.0;
					fSqrDist = fA00+2.0*fB0+fC;
				}
				else if ( fB0 >= 0.0 )
				{
					fS = 0.0;
					fSqrDist = fC;
				}
				else
				{
					fS = -fB0/fA00;
					fSqrDist = fB0*fS+fC;
				}
			}
		}
		else  // region 1
		{
			region = 1;

			fNumer = fA11 + fB1 - fA01 - fB0;
			if ( fNumer <= 0.0 )
			{
				fS = 0.0;
				fT = 1.0;
				fSqrDist = fA11+2.0*fB1+fC;
			}
			else
			{
				fDenom = fA00-2.0*fA01+fA11;
				if ( fNumer >= fDenom )
				{
					fS = 1.0;
					fT = 0.0;
					fSqrDist = fA00+2.0*fB0+fC;
				}
				else
				{
					fS = fNumer/fDenom;
					fT = 1.0 - fS;
					fSqrDist = fS*(fA00*fS+fA01*fT+2.0*fB0) +
						fT*(fA01*fS+fA11*fT+2.0*fB1)+fC;
				}
			}
		}
	}
	result.distance = sqrt(fSqrDist);	
	result.region = region;

	return result;
}
*/
