#include <cuda.h>
#include <cuda_runtime.h>
#include "Vector3.h"
#include "Vertex.h"
#include "HScommon.h"

#ifndef ___TRIANGLE___HHH___
#define ___TRIANGLE___HHH___

/*
struct MyNeighbor
{
	Triangle  *	neighborT[3];
};
*/
class Triangle
{
public:

	Vector3			m_v[3];
	unsigned int	m_ui[3];
	Vector3			m_e[3];
	Vector3			m_normals;
	char			m_flag;
	
	unsigned int	m_uiNidx[3];
	unsigned int	m_uiMe;


	//char makeFlagZero();

	__host__ __device__ Triangle();

	__host__ __device__ Vertex * v (unsigned int index, Vertex * verts)
	{
		unsigned int a = m_ui[index];
		return &verts[a]; 
	}
};

#endif // !___TRIANGLE___HHH___